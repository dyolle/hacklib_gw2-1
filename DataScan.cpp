#include "main.h"
#include "hacklib/PatternScanner.h"
#include "hacklib/Logging.h"

#define VERIFY_CB [](uintptr_t o, uintptr_t val)->bool
#define SHIFT_CB [](uintptr_t o)->uintptr_t

namespace GW2LIB {

    /*
    * DataScan():
    *  pattern = the pattern to search for, passed to hl::FindPattern() or hl::PatternScanner::find()
    *  shiftCb = custom func to call for shifting the offset, "shift" and "size" are ignored
    *  shift = the relative offset from the pattern to the target value
    *  size = number of bytes to extract from the target offset
    *  isString = which scan type to use hl::FindPattern() or hl::PatternScanner::find()
    *  verify = callback to verify if offset is correct
    */
    uintptr_t DataScan(const char *pattern, uintptr_t(*shiftCb)(uintptr_t offset), int32_t shift, uint8_t size, int instance, bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        static std::unordered_map <std::string, uintptr_t> OffsetScanMap;
        uintptr_t val = 0;
        uintptr_t offset = 0;

        if (OffsetScanMap.count(pattern)) {
            offset = OffsetScanMap[pattern];
        } else {
            try {
                if (isString) {
                    static hl::PatternScanner scanner;
                    offset = scanner.findString(pattern, "", instance);
                } else {
                    offset = hl::FindPattern(pattern, "", instance);
                }

                OffsetScanMap[pattern] = offset;
            } catch (...) {
                offset = 0;
            }
        }

        if (!offset) {
            HL_LOG_ERR("[GW2LIB::DataScan] Could not find pattern: \"%s\"\n", pattern);
            throw STATUS_ACCESS_VIOLATION;
        }

        if (shiftCb) { // PointerScan
            try {
                val = shiftCb(offset);
            } catch (...) {
                HL_LOG_ERR("[GW2LIB::DataScan] Exception occured in callback for pattern: \"%s\"\n", pattern);
                throw STATUS_ACCESS_VIOLATION;
            }
        } else { // OffsetScan
            offset += shift;

            // extract target value and byte-swap
            while (size--) val |= (*(uint8_t*)(offset + size)) << (size * 8);
        }

        if (verify && !verify(offset, val)) {
            HL_LOG_ERR("[GW2LIB::DataScan] Verifying pattern failed (offset: 0x%p, value: 0x%X, pattern: \"%s\")\n", offset, val, pattern);
            throw STATUS_ACCESS_VIOLATION;
        }

        return val;
    }

    uintptr_t OffsetScan(const char *pattern, int32_t shift, uint8_t size, int instance, bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        return DataScan(pattern, nullptr, shift, size, instance, isString, verify);
    }

    uintptr_t PointerScan(const char *pattern, uintptr_t(*cb)(uintptr_t offset), int instance, bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        return DataScan(pattern, cb, 0, 0, instance, isString, verify);
    }
};

bool Gw2HackMain::ScanForOffsets() {
    using namespace GW2LIB;
    GW2LIB::Mems &m = m_pubmems;

    try {
#ifdef ARCH_64BIT
        m.contextChar = OffsetScan("!IsPlayer() || GetPlayer()", 0x11, 4, 0, true, VERIFY_CB{ return val >= 0x10 && val <= 0x300; });
        m.avagVtGetAgent = OffsetScan("wmAgent->GetAgent() != agent", -0x16, 1, 0, true, VERIFY_CB{ return val >= 0x24 && val <= 0xc0; });
        m.wmAgVtGetCodedName = OffsetScan("48 85 C0 74 ?? 48 8B 10 48 8B C8 FF 52 ?? 48 85 C0 74 ?? 48 8B D0 48 8B CB E8 ?? ?? ?? ?? 48 8B D8", 0xd, 1, 0, false, VERIFY_CB{ return val >= 0x30 && val <= 0xc0; });
        m.agentVtGetCategory = OffsetScan("agent->GetCategory() == AGENT_CATEGORY_CHAR", -0x15, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x50; });
        m.agentVtGetId = OffsetScan("targetAgent && targetAgent->GetAgentId()", -0x18, 4, 0, true, VERIFY_CB{ return val >= 0x40 && val <= 0x300; });
        m.agentVtGetType = OffsetScan("m_outOfRangeActivationTargetAgent->GetType() == AGENT_TYPE_GADGET_ATTACK_TARGET", -0x19, 4, 0, true, VERIFY_CB{ return val >= 0x80 && val <= 0x400; });
        m.agentVtGetPos = OffsetScan("48 8B 06 48 8B D5 48 8B CE FF 90 ?? ?? ?? ?? 48 8B C5 48 8B 6C 24 ?? 48 83 C4", 0xb, 4, 1, false, VERIFY_CB{ return val >= 0x40 && val <= 0x400; });
        m.gd_agentVtGetToken = OffsetScan("49 8B 16 49 8B CE 48 8B D8 48 8B 38 FF 92 ?? ?? ?? ?? 48 8B D0 48 8B CB FF 57 ?? 48 8B F8 48 85 C0 75", 0xe, 4, 0, false, VERIFY_CB{ return val >= 0xd0 && val <= 0x500; });
        m.gd_agentVtGetSeq = OffsetScan("40 53 48 83 EC 20 48 8B D9 48 8D 54 24 30 48 8B 49 38 48 8B 01 FF 90", 0x17, 4, 0, false, VERIFY_CB{ return val >= 0xf0 && val <= 0x500; });
        m.agentTransform = OffsetScan("85 C0 74 ?? 48 8B 4F ?? 48 8D 54 24 ?? 48 8B 01 FF 50 ?? 48 8B C8 EB ?? 48 C7 44", 7, 1, 0, false, VERIFY_CB{ return val > 0 && val <= 0x70; });
        m.agtransX = OffsetScan("F3 0F 10 59 ?? F3 0F 10 51 ?? F3 0F 10 49 ?? 8B 41 ?? F3 0F 59", 4, 1, 0, false, VERIFY_CB{ return val > 0 && val <= 0x74; });
        m.agtransY = m.agtransX + 4;
        m.agtransZ = m.agtransX + 8;
        m.gd_agtransVtGetRot = OffsetScan("48 83 EC 48 48 8B 49 ?? 48 8D 54 24 ?? 48 8B 01 FF 90 ?? ?? ?? ?? 48 8B D0", 0x12, 4, 0, false, VERIFY_CB{ return val > 0x70 && val <= 0x400; });
        m.agtransVtGetMoveRot = OffsetScan("48 8B 03 48 8D 54 24 ?? 48 8B CB FF 90 ?? ?? ?? ?? F3 0F 10 48 ?? F3 0F 10 00", 0xd, 4, 0, false, VERIFY_CB{ return val > 0x20 && val <= 0x400; });
        m.agtransVtGetRot = OffsetScan("40 53 48 83 EC 20 48 8B 49 ?? 48 8B DA 48 8B 01 FF 90 ?? ?? ?? ?? 48 8B C3", 0x12, 4, 5, false, VERIFY_CB{ return val > 0x70 && val <= 0x600; });
        m.npc_agtransSpeed = OffsetScan("F3 0F 58 C1 F3 0F 58 C2 E8 ?? ?? ?? ?? 48 83 C4 ?? C3 CC CC F3 0F 10 81 ?? ?? ?? ?? F3 0F 59", 0x18, 4, 0, false, VERIFY_CB{ return val > 0x80 && val <= 0x500; });
        m.agtransRealSpeed = OffsetScan("F3 0F 5C CA F3 0F 59 C8 0F 57 C0 F3 0F 58 CA F3 0F 5F C8 F3 0F 11 8B", 0x17, 4, 0, false, VERIFY_CB{ return val > 0x1a0 && val <= 0x700; });
        m.agtransSpeed    = m.agtransRealSpeed + 4;
        m.agtransMaxSpeed = m.agtransRealSpeed + 8;
        m.charctxCharArray = OffsetScan("m_characterArray.Count() <= agentId || !m_characterArray[agentId]", -0x18, 1, 0, true, VERIFY_CB{ return val >= 0x20 && val <= 0xc0; });
        m.charctxPlayerArray = OffsetScan("m_playerArray.Count() <= playerId || !m_playerArray[playerId]", -0x1b, 4, 0, true, VERIFY_CB{ return val >= 0x20 && val <= 0x400; });
        m.charctxControlled = OffsetScan("CC 48 8B 89 ?? ?? ?? ?? 48 85 C9 74 0B 48 8B 41 ?? 48 83 C1 08 48 FF 20 33 C0 C3 CC", 4, 4, 0, false, VERIFY_CB{ return val > 0x50 && val <= 0x300; });
        m.charVtGetAgent = OffsetScan("character->GetAgent() == m_agent", -0x18, 2, 0, true, VERIFY_CB{ return val == 0x10ff; }) - 0x10ff;
        m.charVtGetAgentId = OffsetScan("m_agent && (m_agent->GetAgentId() == character->GetAgentId() || m_masterCharacter == character)", -0x26, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtGetPlayer = OffsetScan("m_ownerCharacter->GetPlayer() == CharClientContext()->GetControlledPlayer()", -0x26, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x500; });
        m.charVtAlive = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x35, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x40; });
        m.charVtDowned = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x28, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x700; });
        m.charVtInWater = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x18, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x700; });
        m.charVtControlled = OffsetScan("IsControlled()", -0x18, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x700; });
        m.charVtMonster = OffsetScan("IsPlayer() || IsMonster()", -0x18, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x700; });
        m.charVtPlayer = OffsetScan("character->IsPlayer() || character->IsMonsterClone()", -0x28, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x900; });
        m.charVtClone = OffsetScan("character->IsPlayer() || character->IsMonsterClone()", -0x18, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x900; });
        m.charVtRangerPet = OffsetScan("m_kennel", -0x87, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x900; });
        m.charAttitude = OffsetScan("m_attitudeTowardControlled < Content::AFFINITY_ATTITUDES", -0x1a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x200; });
        m.charBreakbar = OffsetScan("48 89 5C 24 ?? 48 89 74 24 ?? 57 48 83 EC 30 48 83 B9 ?? ?? ?? ?? 00 41 8B F9", 0x12, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x200; });
        m.charCoreStats = OffsetScan("m_coreStats", -0x17, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x500; });
        m.charEndurance = OffsetScan("!m_endurance", -0x22, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x600; });
        m.charHealth = OffsetScan("m_health", -0x1a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x800; });
        m.charInventory = OffsetScan("m_inventory", -0x1a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x800; });
        m.charVtGetCmbtnt = OffsetScan("48 8B D6 48 8B C8 4C 8B 00 41 FF 50 ?? 48 8B C8 48 8B 10 48 83 C4 20 5E 48 FF A2", 0x1b, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charSkillbar = OffsetScan("!m_skillbar", -0x1a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x900; });
        m.charGliderPercent = OffsetScan("CC 48 83 EC 28 F3 0F 10 81 ?? ?? ?? ?? 4C 8D 4C 24 ?? F3 0F 11 89 ?? ?? ?? ?? 4C 8D 44 24", 9, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charProfession = OffsetScan("m_profession", -0x1a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x900; });
        m.charName = OffsetScan("TextValidateLiteral(m_name.Ptr())", -0x25, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0xa0; });
        m.charFlags = OffsetScan("CC 48 8B 81 ?? ?? ?? ?? 48 85 C0 74 0E 8B 88 ?? ?? ?? ?? C0 E9 04 F6 C1 01", 0xf, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtAsFlags = OffsetScan("48 8B 03 48 8B CB FF 90 ?? ?? ?? ?? C1 E8 1B 83 E0 01 48 83 C4 20 5B C3", 0x8, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x500; });
        m.charID = OffsetScan("41 B9 01 00 00 00 48 8B 50 ?? 48 8D 48 ?? 4C 63 42 ?? 48 8B 93", 0x15, 4, 0, false, VERIFY_CB{ return val >= 0x30 && val <= 0x300; });
        m.charSubClass = OffsetScan("character->GetAgent() == m_agent", -0x1f, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x30; });
        m.playerName = OffsetScan("TextValidateLiteral(m_name.Ptr())", -0x25, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0xa0; });
        m.playerVtGetWallet = OffsetScan("48 8B 10 FF 92 ?? ?? ?? ?? 48 8B C8 48 8B 10 FF 52 ?? 45 33 C9 44 8B C0", 5, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.playerVtGetTrainMgr = OffsetScan("48 8B 03 48 8B CB FF 90 ?? ?? ?? ?? 48 8B C8 4C 8B 00 41 FF 90 ?? ?? ?? ?? 48 89 47", 0x15, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x600; });
        m.playerVtGetAchMgr = OffsetScan("achievementMgr", -0x1c, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.playerVtGetSpecMgr = OffsetScan("specializationMgr", -0x1c, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x500; });
        m.playerVtGetChar = OffsetScan("CC 48 8B 89 ?? ?? ?? ?? 48 85 C9 74 ?? 48 8B 01 48 FF 60 ?? 33 C0 C3 CC", 0x13, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.cmbtntVtGetBuffBar = OffsetScan("m_direction2d < arrsize(s_stateGroundLimp)", -0x38, 1, 0, true, VERIFY_CB{ return val >= 0 && val <= 0x80; });
        m.buffbarBuffArr = OffsetScan("83 79 ?? 00 48 8B FA 48 8B D9 74 ?? 8B 41 ?? 39 02 73 ?? 0F 1F", 0xe, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x60; });
        m.buffbarBuffStatArr = OffsetScan("48 89 5C 24 ?? 48 8D 99 ?? ?? ?? ?? 48 83 7B ?? 00 89 7C 24 ?? 74", 0x8, 4, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x300; });
        m.buffEfType = OffsetScan("44 8B 50 ?? 48 8D 05 ?? ?? ?? ?? 44 89 51 ?? 48 89 01", 0xe, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffSkillDef = OffsetScan("44 8B 50 ?? 48 8D 05 ?? ?? ?? ?? 44 89 51 ?? 48 89 01", 0x17, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffBuffId = OffsetScan("49 89 7E ?? 48 8B 06 49 89 46 ?? 8B 44 24 ?? 41 89 46 ?? 8B 44 24 ?? 41 89 46 ?? E8", 0x12, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.buffBuffBar = OffsetScan("49 89 7E ?? 48 8B 06 49 89 46 ?? 8B 44 24 ?? 41 89 46 ?? 8B 44 24 ?? 41 89 46 ?? E8", 3, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.buffSrcAg = OffsetScan("33 C0 48 89 41 ?? 48 89 41 ?? 48 89 41 ?? 48 89 41 ?? 48 83 C1 ?? E8", 0x9, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x60; });
        m.buffDuration = OffsetScan("41 89 46 ?? E8 ?? ?? ?? ?? 48 8B 5C 24 ?? 48 8B 74 24 ?? 48 8B 7C 24", 3, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x70; });
        m.buffActive = OffsetScan("41 89 46 ?? E8 ?? ?? ?? ?? 48 8B 5C 24 ?? 48 8B 74 24 ?? 48 8B 7C 24", 0x2b, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x70; });
        m.skillbarSkillsArray = OffsetScan("skillbarSlot < arrsize(m_slotSkillDefs)", 0xd, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.skillbarPressedSkill = OffsetScan("skillbarSlot < CHAR_SKILLBAR_SLOTS", 0xd, 1, 0, true, VERIFY_CB{ return val >= 0 && val <= 0xc0; });
        m.skillbarHoveredSkill = OffsetScan("48 8B 02 48 3B 81 ?? ?? ?? ?? 74 21 48 89 81 ?? ?? ?? ?? 4C 8D 81 ?? ?? ?? ?? 48 81 C1", 6, 4, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x300; });
        m.skillDefEffect = OffsetScan("83 7F ?? 02 44 8B 1D ?? ?? ?? ?? 0F 84 ?? ?? ?? ?? 44 8B 53", 0x14, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x60; });
        m.skillDefInfo = OffsetScan("48 8B 19 83 7B ?? 01 75 ?? 48 8B 7B ?? EB ?? 33 FF", 0xc, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x90; });
        m.healthBarrier = OffsetScan("F3 0F 10 43 ?? 0F 57 C9 0F 2F C1 76 ?? BA ?? ?? ?? ?? 0F 29 74 24", 0x4, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x4c; });
        m.contextGadget = PointerScan("ViewAdvanceGadget", SHIFT_CB{ return *(uint32_t*)(hl::FollowRelativeAddress(o + 0xa) + 0xc); }, 0, true, VERIFY_CB{ return true; });
        m.ctxgdVtGetGadget = OffsetScan("48 8B 38 FF 92 ?? ?? ?? ?? 8B D0 48 8B CB FF 57 ?? 48 8B C8 48 8B F8 48 8B 10 FF 92", 0x19, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.gdHealth = OffsetScan("IsCombatant() && m_healthMeter", -0x17, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.gdVtGetType = OffsetScan("gadget->GetType() == Content::GADGET_TYPE_RESOURCE_NODE", -0x19, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x200; });
        m.gdVtGetRNode = OffsetScan("48 89 5C 24 ?? 57 48 83 EC ?? 48 8B D9 48 8B 89 ?? ?? ?? ?? 48 8B 01 FF 90", 0x19, 4, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x400; });
#else
        m.contextChar = OffsetScan("!IsPlayer() || GetPlayer()", 0x10, 1, 0, true, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.avagVtGetAgent = OffsetScan("wmAgent->GetAgent() != agent", -0x10, 1, 0, true, VERIFY_CB{ return val >= 0x24 && val <= 0x60; });
        m.wmAgVtGetCodedName = OffsetScan("85 C0 74 ?? 8B 10 8B C8 FF 52 ?? 85 C0 74 ?? 8B D0 8B CE E8 ?? ?? ?? ?? 8B F0 8B 4B ?? E8", 0xa, 1, 0, false, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.agentVtGetCategory = OffsetScan("agent->GetCategory() == AGENT_CATEGORY_CHAR", -0xd, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x30; });
        m.agentVtGetId = OffsetScan("targetAgent && targetAgent->GetAgentId()", -0x10, 1, 0, true, VERIFY_CB{ return val >= 0x40 && val <= 0x90; });
        m.agentVtGetType = OffsetScan("m_outOfRangeActivationTargetAgent->GetType() == AGENT_TYPE_GADGET_ATTACK_TARGET", -0x14, 4, 0, true, VERIFY_CB{ return val >= 0x80 && val <= 0x200; });
        m.agentVtGetPos = OffsetScan("8B 03 8B CB 56 FF 90 ?? ?? ?? ?? 8B C6 5E 5B 8B E5 5D C3", 7, 4, 1, false, VERIFY_CB{ return val >= 0x40 && val <= 0x200; });
        m.gd_agentVtGetToken = OffsetScan("8B F0 8B 45 FC 8B C8 8B 10 8B 3E 8B 92 ?? ?? ?? ?? FF D2 52 50 8B CE FF 57", 0xd, 4, 0, false, VERIFY_CB{ return val >= 0xd0 && val <= 0x300; }); // L"Content is referencing a propID"
        m.gd_agentVtGetSeq = OffsetScan("55 8B EC 83 EC 10 8D 55 F8 56 8B F1 52 8B 4E ?? 8B 01 FF 90 ?? ?? ?? ?? 8B 4E ?? 85 C9", 0x14, 4, 0, false, VERIFY_CB{ return val >= 0xf0 && val <= 0x300; });
        m.agentTransform = OffsetScan("85 C0 74 ?? 8B 4E ?? 8D 55 ?? 52 8B 01 FF 50 ?? 8B D0 EB ?? C7 45 ?? 00 00 00 00 8D 55 ?? C7 45 ?? 00 00 00 00 C7 45", 6, 1, 0, false, VERIFY_CB{ return val > 0 && val <= 0x40; });
        m.agtransX = OffsetScan("55 8B EC F3 0F 10 51 ?? F3 0F 10 49 ?? F3 0F 10 41 ?? 8B 45 ?? F3 0F 59 15", 7, 1, 0, false, VERIFY_CB{ return val > 0 && val <= 0x44; });
        m.agtransY = m.agtransX + 4;
        m.agtransZ = m.agtransX + 8;
        m.gd_agtransVtGetRot = OffsetScan("55 8B EC 83 EC 20 8B 49 ?? 8D 55 ?? 52 8B 01 8B 80 ?? ?? ?? ?? FF D0 8B D0 8D 4D ?? E8", 0x11, 4, 0, false, VERIFY_CB{ return val > 0x70 && val <= 0x200; });
        m.agtransVtGetMoveRot = OffsetScan("8B 06 8D 4D ?? 51 8B CE 8B 40 ?? FF D0 51 F3 0F 10 48", 0xa, 1, 0, false, VERIFY_CB{ return val > 0x20 && val <= 0x100; });
        m.agtransVtGetRot = OffsetScan("55 8B EC 8B 49 ?? FF 75 08 8B 11 FF 92 ?? ?? ?? ?? 8B 45 08 5D C2 04 00", 0xd, 4, 6, false, VERIFY_CB{ return val > 0x70 && val <= 0x200; });
        m.npc_agtransSpeed = OffsetScan("F3 0F 58 D1 F3 0F 58 D0 F3 0F 11 14 24 E8 ?? ?? ?? ?? 8B E5 5D C3 D9 81 ?? ?? ?? ?? D8 0D", 0x18, 4, 0, false, VERIFY_CB{ return val > 0x80 && val <= 0x200; });
        m.agtransRealSpeed = OffsetScan("F3 0F 5C C2 F3 0F 59 C1 F3 0F 58 C2 F3 0F 5F 05 ?? ?? ?? ?? F3 0F 11 86", 0x18, 4, 0, false, VERIFY_CB{ return val > 0x1a0 && val <= 0x400; });
        m.agtransSpeed    = m.agtransRealSpeed + 4;
        m.agtransMaxSpeed = m.agtransRealSpeed + 8;
        m.charctxCharArray = OffsetScan("m_characterArray.Count() <= agentId || !m_characterArray[agentId]", -0x12, 1, 0, true, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.charctxPlayerArray = OffsetScan("m_playerArray.Count() <= playerId || !m_playerArray[playerId]", -0x12, 1, 0, true, VERIFY_CB{ return val >= 0x40 && val <= 0x80; });
        m.charctxControlled = OffsetScan("CC 8B 49 ?? 85 C9 74 ?? 83 C1 04 8B 01 FF 20 33 C0 C3 CC", 3, 1, 0, false, VERIFY_CB{ return val > 0x50 && val <= 0x90; });
        m.charVtGetAgent = OffsetScan("character->GetAgent() == m_agent", -0x12, 2, 0, true, VERIFY_CB{ return val == 0x10ff; }) - 0x10ff;
        m.charVtGetAgentId = OffsetScan("m_agent && (m_agent->GetAgentId() == character->GetAgentId() || m_masterCharacter == character)", -0x19, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x300; });
        m.charVtGetPlayer = OffsetScan("m_ownerCharacter->GetPlayer() == CharClientContext()->GetControlledPlayer()", -0x1f, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtAlive = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x2c, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x40; });
        m.charVtControlled = OffsetScan("IsControlled()", -0x13, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtDowned = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x21, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtInWater = OffsetScan("character->IsAlive() || (character->IsDowned() && character->IsInWater())", -0x13, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtMonster = OffsetScan("IsPlayer() || IsMonster()", -0x13, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtClone = OffsetScan("character->IsPlayer() || character->IsMonsterClone()", -0x13, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtPlayer = OffsetScan("character->IsPlayer() || character->IsMonsterClone()", -0x25, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtRangerPet = OffsetScan("m_kennel", -0x6a, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charAttitude = OffsetScan("m_attitudeTowardControlled < Content::AFFINITY_ATTITUDES", -0xf, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0xc0; });
        m.charBreakbar = OffsetScan("55 8B EC 8B 49 ?? 85 C9 74 ?? 8B 55 ?? 56 8B 31 0F B6 42", 5, 1, 0, false, VERIFY_CB{ return val > 0 && val <= 0xa0; });
        m.charCoreStats = OffsetScan("m_coreStats", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charEndurance = OffsetScan("!m_endurance", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charHealth = OffsetScan("m_health", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charInventory = OffsetScan("m_inventory", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtGetCmbtnt = OffsetScan("53 8B C8 8B 10 FF 52 ?? 8B C8 8B 10 FF 92 ?? ?? ?? ?? 5B 5D C2 04 00", 0xe, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charSkillbar = OffsetScan("!m_skillbar", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charGliderPercent = OffsetScan("55 8B EC 51 F3 0F 10 81 ?? ?? ?? ?? 8D 45 FC 50", 8, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charProfession = OffsetScan("m_profession", -0x12, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charName = OffsetScan("TextValidateLiteral(m_name.Ptr())", -0x1f, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x90; });
        m.charFlags = OffsetScan("CC 8B 41 ?? 85 C0 74 ?? 8B 88 ?? ?? ?? ?? 83 E1 10 83 C9 00 75 ?? 33 C0 C3 CC", 0xa, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charVtAsFlags = OffsetScan("8B 06 8B CE FF 90 ?? ?? ?? ?? C1 E8 1B 83 E0 01 5E C3", 0x6, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.charID = OffsetScan("56 8B F1 E8 ?? ?? ?? ?? 6A 01 6A 00 FF 76 ?? 8B 50 ?? 8D 48 ?? 8B 52 ?? 03 CA 8B 01 FF 50", 0xe, 1, 0, false, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.charSubClass = OffsetScan("character->GetAgent() == m_agent", -0x15, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x50; });
        m.playerName = OffsetScan("TextValidateLiteral(m_name.Ptr())", -0x1f, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0xa0; });
        m.playerVtGetWallet = OffsetScan("8B C8 8B 10 FF 52 ?? 8B C8 8B 10 FF 92 ?? ?? ?? ?? 8B C8 8B 10 FF 52 ?? 6A 00 50 BA", 0xd, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.playerVtGetTrainMgr = OffsetScan("8B 06 8B CE FF 90 ?? ?? ?? ?? 8B C8 8B 30 FF 96 ?? ?? ?? ?? 89 47 ?? 85 C0 75", 0x10, 4, 0, false, VERIFY_CB{ return val > 0 && val <= 0x400; }); // "m_trainingMgr"
        m.playerVtGetAchMgr = OffsetScan("achievementMgr", -0x15, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.playerVtGetSpecMgr = OffsetScan("specializationMgr", -0x15, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.playerVtGetChar = OffsetScan("8B CE FF 90 ?? ?? ?? ?? 8B C8 8B ?? FF 52 10 3B C6 0F 85", 0xe, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.cmbtntVtGetBuffBar = OffsetScan("m_direction2d < arrsize(s_stateGroundLimp)", -0x2a, 1, 0, true, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.buffbarBuffArr = OffsetScan("8B F9 8D 4F ?? E8 ?? ?? ?? ?? 85 C0 75 ?? 5F", 4, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffbarBuffStatArr = OffsetScan("00 57 8D 78 ?? 89 75 ?? 74 ?? 8D 45 ?? 8B CF 50", 4, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x80; });
        m.buffEfType = OffsetScan("55 8B EC 8B 45 ?? 56 8B F1 8B 40 ?? 89 46 ?? 8D 4E ?? C7 06", 0xe, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffSkillDef = OffsetScan("55 8B EC 8B 45 ?? 56 8B F1 8B 40 ?? 89 46 ?? 8D 4E ?? C7 06", 0x1a, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffBuffId = OffsetScan("8B 45 0C 89 46 ?? 8B 45 10 89 46 ?? 8B 45 14 89 46 ?? 8B 45 18 89 46 ?? E8", 0x11, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffBuffBar = OffsetScan("8B 45 0C 89 46 ?? 8B 45 10 89 46 ?? 8B 45 14 89 46 ?? 8B 45 18 89 46 ?? E8", 0x5, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.buffSrcAg = OffsetScan("C7 06 ?? ?? ?? ?? C7 46 ?? 00 00 00 00 C7 46 ?? 00 00 00 00 C7 46 ?? 00 00 00 00 C7 46 ?? 00 00 00 00 E8 ?? ?? ?? ?? FF 75 ??", 0xf, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x50; });
        m.buffDuration = OffsetScan("89 46 ?? E8 ?? ?? ?? ?? 89 46 ?? 8B 45 ?? 89 46 ?? 8B 45 ?? 89 46 ?? 8B C6 5E 5D C2", 2, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x70; });
        m.buffActive = OffsetScan("89 46 ?? E8 ?? ?? ?? ?? 89 46 ?? 8B 45 ?? 89 46 ?? 8B 45 ?? 89 46 ?? 8B C6 5E 5D C2", 0x16, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x70; });
        m.skillbarSkillsArray = OffsetScan("skillbarSlot < arrsize(m_slotSkillDefs)", 0xc, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.skillbarPressedSkill = OffsetScan("skillbarSlot < CHAR_SKILLBAR_SLOTS", 0xd, 1, 0, true, VERIFY_CB{ return val >= 0 && val <= 0x80; });
        m.skillbarHoveredSkill = OffsetScan("55 8B EC 8B 45 08 3B 41 ?? 74 ?? 89 41 ?? 8D 81", 8, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x90; });
        m.skillDefEffect = OffsetScan("85 C9 0F 85 ?? ?? ?? ?? 8B 76 ?? 8D 45 ?? 50 8D 45", 0xa, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x60; });
        m.skillDefInfo = OffsetScan("8B 30 83 7E ?? 01 75 ?? 8B 5E ?? EB ?? 33 DB", 0xa, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x90; });
        m.healthBarrier = OffsetScan("F3 0F 10 46 ?? 0F 57 C9 0F 2F C1 76 ?? BA ?? ?? ?? ?? 8D 4A", 0x4, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x4c; });
        m.contextGadget = PointerScan("ViewAdvanceGadget", SHIFT_CB{ return *(uintptr_t*)(hl::FollowRelativeAddress(o + 0xa) + 7); }, 0, true, VERIFY_CB{ return val >= 0 && val <= 0x300; });
        m.ctxgdVtGetGadget = OffsetScan("8B 3E FF 52 ?? 50 8B CE FF 57 ?? 8B C8 8B 10 FF 52 ?? 8B 75", 0xa, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        m.gdHealth = OffsetScan("IsCombatant() && m_healthMeter", -0xf, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });
        m.gdVtGetType = OffsetScan("gadget->GetType() == Content::GADGET_TYPE_RESOURCE_NODE", -0x11, 1, 0, true, VERIFY_CB{ return val > 0 && val <= 0x90; });
        m.gdVtGetRNode = OffsetScan("56 8B F1 57 8B 4E ?? 8B 01 FF 90 ?? ?? ?? ?? 8B 4E", 0xb, 1, 2, false, VERIFY_CB{ return val >= 0 && val <= 0xb0; });
        //m. = OffsetScan("", 0x0, 1, 0, false, VERIFY_CB{ return val >= 0 && val <= 0x40; });
        //m. = OffsetScan("", 0x0, 4, 0, true, VERIFY_CB{ return val > 0 && val <= 0x400; });

#endif
    } catch (...) {
        return false;
    }

    return true;
}

bool Gw2HackMain::ScanForPointers() {
    using namespace GW2LIB;
    GamePointers &m = m_mems;

    try {
#ifdef ARCH_64BIT
        m.avctxAgentArray = PointerScan("48 85 C0 74 ?? 48 8B 07 33 D2 48 8B CF FF 90 ?? ?? ?? ?? 85 C0 74", SHIFT_CB{
            return hl::FollowRelativeAddress(hl::FollowRelativeAddress(hl::FollowRelativeAddress(o - 4) + 0xa) + 3);
        }, 0, false, VERIFY_CB{ return true; }) + 8;
#else
        m.avctxAgentArray = PointerScan("85 C0 74 ?? 8B 07 8B CF 6A 00 FF 90 ?? ?? ?? ?? 85 C0 74", SHIFT_CB{
            return *(uintptr_t*)(hl::FollowRelativeAddress(hl::FollowRelativeAddress(o - 4) + 2) + 1);
        }, 0, false, VERIFY_CB{ return true; }) + 4;
#endif
    } catch (...) {
        return false;
    }

    return true;
}


